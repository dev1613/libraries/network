/**
 * @file Socket.h
 *
 * @brief Represents a cross-platform socket connection
 *
 * @ingroup Network
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <atomic>
#include <functional>
#include <memory>
#include <thread>

#include <Network/Common.h>
#include <System/Logger.h>
#include <System/SerialBuffer.h>

namespace Network
{
  //-------------------------------------------------------------------------//
  // Class:       SocketSystem
  // Description: Singleton to do one-time initialization tasks
  //-------------------------------------------------------------------------//
  class SocketSystem
  {
  public:
    // Public constants
    static constexpr int ERR_SUCCESS = 0;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~SocketSystem();

  private:
    friend class Socket;

    // Private class variables
    int mErrorCode; ///< error code

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    SocketSystem();
  };

  //-------------------------------------------------------------------------//
  // Class:       SocketStatus
  // Description: Availability status for a socket
  //-------------------------------------------------------------------------//
  struct SocketStatus
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - logger
    /// @param port - port number
    /// @param hostname - hostname, or nullptr for server socket
    ///////////////////////////////////////////////////////////////////////////
    SocketStatus() :
      error(false),
      read(false),
      write(false),
      except(false)
    {
    }

    // Struct variables
    bool error;  ///< error retrieving status
    bool read;   ///< available for read
    bool write;  ///< available for writing
    bool except; ///< exception occurred on socket
  };

  //-------------------------------------------------------------------------//
  // Class:       Socket
  // Description: Represents a cross-platform socket
  //-------------------------------------------------------------------------//
  class Socket
  {
  public:
    // Public types
    using AcceptCallback = std::function<void(Network::sSocket)>;
    using DataCallback   = std::function<void(Socket*, System::sSerialBuffer)>;

    // Constants
    static inline const timeval DEFAULT_TIMEOUT = {0, 30000};

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - logger
    /// @param acceptCallback - function to call when new connection accepted
    /// @param dataCallback - function to call when data received
    /// @param port - port number
    /// @param hostname - hostname, or nullptr for server socket
    ///////////////////////////////////////////////////////////////////////////

    // Server
    Socket(System::sLogger pLogger, const AcceptCallback& acceptCallback, const DataCallback& dataCallback, uint32_t port);

    // Client
    Socket(System::sLogger pLogger, const DataCallback& dataCallback, uint32_t port, const std::string& hostname);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Socket();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Checks if data is available on this socket
    /// @param timeout - timeout to stop waiting
    /// @param write - if true, check for writable
    /// @return - socket status
    ///////////////////////////////////////////////////////////////////////////
    SocketStatus available(const timeval& timeout = DEFAULT_TIMEOUT);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Closes the connection if open
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void closeConnection();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Checks if this is a server socket
    /// @return - true if server socket
    ///////////////////////////////////////////////////////////////////////////
    bool isServerSocket() const { return mServer; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sends a message on this socket
    /// @param msg - message to send
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool sendMsg(const System::SerialBuffer& msg);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Checks if the socket is valid
    /// @return true if valid, else false
    ///////////////////////////////////////////////////////////////////////////
    bool valid() const { return(mSocketID != INVALID_SOCKET); }

  private:
    // Private class variables
    const System::sLogger mLogger;   ///< debug logger
    const bool            mServer;   ///< server socket?

    const AcceptCallback mAcceptCallback; ///< callback function for new connection
    const DataCallback   mDataCallback;   ///< callback function for received data

    SocketID          mSocketID;   ///< socket ID
    std::atomic<bool> mRunning;    ///< receive thread running?
    std::thread       mRecvThread; ///< background thread for receiving data

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param callback - function to call when data received
    /// @param socketID - socket ID
    ///////////////////////////////////////////////////////////////////////////
    Socket(System::sLogger pLogger, const DataCallback& callback, SocketID socketID);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Thread for accepting new connections
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void acceptThread();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the socket
    /// @param port - port number
    /// @param hostname - hostname, or nullptr for server socket
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initialize(uint32_t port, const char* hostname = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Thread for receiving data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void recvThread();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets whether the connection is blocking
    /// @param blocking - true for blocking
    /// @return - 0 if successful, else SOCKET_ERROR
    ///////////////////////////////////////////////////////////////////////////
    int setBlocking(bool blocking);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Starts the receiving thread
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void startRecvThread();
  };

} // namespace Network
