/**
 * @file Common.h
 *
 * @brief Defines all the platform-specific socket types and constants
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <memory>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#endif

namespace Network
{
  // Platform-specific types, constants, and functions
#ifdef _WIN32
  using SocketID = SOCKET;
  using AddrInfo = ADDRINFOT;
  using BufferType = char;

  constexpr int ERR_WOULD_BLOCK = WSAEWOULDBLOCK;

  inline int GetLastError() { return WSAGetLastError(); }
#else
  using SocketID = int;
  using AddrInfo = struct addrinfo;
  using BufferType = void;

  constexpr int INVALID_SOCKET  = -1;
  constexpr int SOCKET_ERROR    = -1;
  constexpr int ERR_WOULD_BLOCK = EAGAIN;

  inline int GetLastError() { return errno; }
#endif

  // Forward declarations
  class Socket;

  using sSocket = std::shared_ptr<Socket>;

  using scSocket = std::shared_ptr<const Socket>;

} // namespace Network
