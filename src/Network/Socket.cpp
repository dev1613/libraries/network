/**
 * @file Socket.cpp
 *
 * @brief Represents a cross-platform socket connection
 *
 * @ingroup Network
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <algorithm>
#include <string>

#include <Network/Socket.h>

// Constants
static const unsigned long ON_ULONG = 1;
static const int           ON_INT   = 1;
static const char * const  ON_PTR   = reinterpret_cast<const char*>(&ON_INT);

namespace Network
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor (SocketSystem)
  /////////////////////////////////////////////////////////////////////////////
  SocketSystem::SocketSystem()
  {
#ifdef _WIN32
    WSADATA wsaData;
    mErrorCode = WSAStartup(MAKEWORD(2,2), &wsaData);
#else
    mErrorCode = ERR_SUCCESS;
#endif
  }

  /////////////////////////////////////////////////////////////////////////////
  // Destructor (SocketSystem)
  /////////////////////////////////////////////////////////////////////////////
  SocketSystem::~SocketSystem()
  {
#ifdef _WIN32
    WSACleanup();
#endif
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Socket::Socket(System::sLogger pLogger, const DataCallback& callback, SocketID socketID) :
    mLogger(pLogger), mServer(false), mDataCallback(callback),
    mSocketID(socketID), mRunning(false)
  {
    startRecvThread();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Socket::Socket(System::sLogger pLogger, const AcceptCallback& acceptCallback, const DataCallback& dataCallback, uint32_t port) :
    mLogger(pLogger), mServer(true),
    mAcceptCallback(acceptCallback), mDataCallback(dataCallback)
  {
    initialize(port);
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Socket::Socket(System::sLogger pLogger, const DataCallback& dataCallback, uint32_t port, const std::string& hostname) :
    mLogger(pLogger), mServer(false), mDataCallback(dataCallback)
  {
    initialize(port, hostname.c_str());
  }

  /////////////////////////////////////////////////////////////////////////////
  // Destructor
  /////////////////////////////////////////////////////////////////////////////
  Socket::~Socket()
  {
    closeConnection();
    if(mRecvThread.joinable())
    {
      mRecvThread.join();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // available
  /////////////////////////////////////////////////////////////////////////////
  SocketStatus Socket::available(const timeval& timeout)
  {
    SocketStatus status;
    if(!valid())
    {
      mLogger->error("Socket::%s - Invalid socket ID", __func__);
      status.error = true;
    }
    else
    {
      fd_set fdSetRead, fdSetWrite, fdSetExcept;
      FD_ZERO(&fdSetRead);
      FD_SET(mSocketID, &fdSetRead);

      fdSetWrite  = fdSetRead;
      fdSetExcept = fdSetRead;

      int error = select(mSocketID + 1, &fdSetRead, &fdSetWrite, &fdSetExcept, &timeout);

      if(error < 0)
      {
        mLogger->error("Socket::%s - select failed with error %d", __func__, GetLastError());
        status.error = true;
      }
      else
      {
        if(FD_ISSET(mSocketID, &fdSetRead))
        {
          status.read = true;
        }

        if(FD_ISSET(mSocketID, &fdSetWrite))
        {
          status.write = true;
        }

        if(FD_ISSET(mSocketID, &fdSetExcept))
        {
          status.except = true;
        }
      }
    }

    return status;
  }

  /////////////////////////////////////////////////////////////////////////////
  // closeConnection
  /////////////////////////////////////////////////////////////////////////////
  void Socket::closeConnection()
  {
    if(valid())
    {
      // Stop receive thread
      mRunning = false;

      // Close socket
#ifdef _WIN32
      shutdown(mSocketID, SD_BOTH);
      closesocket(mSocketID);
#else
      shutdown(mSocketID, SHUT_RDWR);
      close(mSocketID);
#endif

      // Invalidate socket ID
      mSocketID = INVALID_SOCKET;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // sendMsg
  /////////////////////////////////////////////////////////////////////////////
  bool Socket::sendMsg(const System::SerialBuffer& msg)
  {
    bool success = false;
    if(!valid())
    {
      mLogger->error("Socket::%s - Invalid socket", __func__);
    }
    else if(mServer)
    {
      mLogger->error("Socket::%s - Server socket should only be used to accept connections", __func__);
    }
    else
    {
      auto& buffer = msg.buffer();
      size_t bytes = buffer.size();
      auto* ptr = reinterpret_cast<const BufferType*>(buffer.data());
      if(send(mSocketID, ptr, bytes, 0) == bytes)
      {
        success = true;
      }
      else
      {
        mLogger->error("Socket::%s - send failed with error %d",
          __func__, GetLastError());
      }
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // acceptThread
  /////////////////////////////////////////////////////////////////////////////
  void Socket::acceptThread()
  {
    // Only applicable for server sockets
    if(!mServer)
    {
      mLogger->error("Socket::%s - Tried to launch with client socket", __func__);
      return;
    }

    while(mRunning.load())
    {
      // Wait for new connection to be available
      if(!available().read)
      {
        continue;
      }

      // Accept new connection
      SocketID socketID = accept(mSocketID, nullptr, nullptr);
      if(socketID == INVALID_SOCKET)
      {
        mLogger->error("Socket::%s - Failed to accept connection with error %d",
          __func__, GetLastError());
      }
      else
      {
        sSocket pSocket(new Socket(mLogger, mDataCallback, socketID));
        mLogger->debug("Socket::%s - Accepted new server connection", __func__);

        if(mAcceptCallback)
        {
          mAcceptCallback(pSocket);
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // initialize
  /////////////////////////////////////////////////////////////////////////////
  void Socket::initialize(uint32_t port, const char* hostname)
  {
    // Initialize class variables
    mSocketID = INVALID_SOCKET;
    mRunning  = false;

    // Initialize system
    static SocketSystem SYSTEM;

    // Check startup
    if(SYSTEM.mErrorCode != SocketSystem::ERR_SUCCESS)
    {
      mLogger->fatal("SocketSystem startup failed with error code %d", SYSTEM.mErrorCode);
      return;
    }

    // Resolve address and port
    const char* hostnameLog = (hostname ? hostname : "NULL");
    AddrInfo *pResult = nullptr;
    AddrInfo addrHint;

    memset(&addrHint, 0, sizeof(addrHint));
    addrHint.ai_family   = AF_INET;
    addrHint.ai_socktype = SOCK_STREAM;
    addrHint.ai_protocol = IPPROTO_TCP;
    addrHint.ai_flags    = AI_PASSIVE;

    int error = getaddrinfo(hostname, std::to_string(port).c_str(), &addrHint, &pResult);
    if(error != SocketSystem::ERR_SUCCESS)
    {
      mLogger->error("Socket: getaddrinfo failed with error %d for %s:%u", error, 
                      hostnameLog, port);
      return;
    }

    // Attempt to create socket until one succeeds
    for(auto* ptr = pResult; ptr && !valid(); ptr = ptr->ai_next)
    {
      // Create socket for connection
      mSocketID = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
      if(mSocketID == INVALID_SOCKET)
      {
        mLogger->warn("Failed to create socket for %s:%u", hostnameLog, port);
        continue;
      }

      // Allow socket ID to be reusable
      if(setsockopt(mSocketID, SOL_SOCKET, SO_REUSEADDR, ON_PTR, sizeof(ON_INT)) == SOCKET_ERROR)
      {
        mLogger->warn("Failed to set socket reusable for %s:%u", hostnameLog, port);
        closeConnection();
        continue;
      }

      // Set socket non-blocking
      if(setBlocking(false) == SOCKET_ERROR)
      {
        mLogger->warn("Failed to set non-blocking for %s:%u", hostnameLog, port);
        closeConnection();
        continue;
      }

      // Check if server or client
      if(mServer)
      {
        // Bind listening socket
        if(bind(mSocketID, ptr->ai_addr, ptr->ai_addrlen) == SOCKET_ERROR)
        {
          mLogger->warn("Failed to bind listening socket on port %u with error %d",
                        port, GetLastError());
          closeConnection();
          continue;
        }

        // Enable listening socket
        if(listen(mSocketID, SOMAXCONN) == SOCKET_ERROR)
        {
          mLogger->warn("Failed to enable listening socket with %d backlog on port %u with error %d",
                         SOMAXCONN, port, GetLastError());
          closeConnection();
          continue;
        }
      }
      else
      {
        // Connect to server socket
        if(connect(mSocketID, ptr->ai_addr, ptr->ai_addrlen) == SOCKET_ERROR)
        {
          if(GetLastError() != ERR_WOULD_BLOCK || !available().write)
          {
            mLogger->warn("Failed to connect to %s:%u with error %d",
                          hostnameLog, port, GetLastError());
            closeConnection();
            continue;
          }
        }
      }
    }

    // Initialize socket
    freeaddrinfo(pResult);

    // Start recv thread
    if(valid())
    {
      startRecvThread();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // recvThread
  /////////////////////////////////////////////////////////////////////////////
  void Socket::recvThread()
  {
    // Only applicable for server sockets
    if(mServer)
    {
      mLogger->error("Socket::%s - Tried to launch with server socket", __func__);
      return;
    }

    // Local variables
    size_t cacheSize = 0;
    System::MsgHeader header;
    System::sSerialBuffer pBuffer;
    std::vector<uint8_t> cache(System::SerialBuffer::DEFAULT_SIZE);

    while(mRunning.load() && valid())
    {
      // Receive next chunk from socket
      if(cacheSize < sizeof(header))
      {
        // Wait for data to be available
        if(available().read)
        {
          size_t bytesAvailable = cache.size() - cacheSize;
          auto* ptr = reinterpret_cast<BufferType*>(&cache[cacheSize]);
          int error = recv(mSocketID, ptr, bytesAvailable, 0);
          if(error < 0)
          {
            mLogger->error("Socket::%s - Failed to receive data with error %d", __func__, GetLastError());
            closeConnection();
            continue;
          }
          else if(error == 0)
          {
            mLogger->debug("Socket::%s - Connection closed by peer", __func__);
            closeConnection();
            continue;
          }

          cacheSize += static_cast<size_t>(error);
        }
        else
        {
          // No data available
          continue;
        }
      }

      // Check for start of new message
      size_t offset = 0;
      if(pBuffer == nullptr)
      {
        if(cacheSize < sizeof(header))
        {
          // Haven't yet received header for next message
          continue;
        }
        else
        {
          header = *reinterpret_cast<System::MsgHeader*>(cache.data());
          header = header.networkToHost();

          pBuffer = std::make_shared<System::SerialBuffer>(header.length);
          pBuffer->setMsgID(header.id);

          offset = sizeof(header);
          header.length = std::max<uint32_t>(header.length, sizeof(header));
        }
      }

      // Push new data into buffer
      size_t bytes = std::min<size_t>(header.length, cacheSize);
      pBuffer->append(&cache[offset], bytes - offset);

      // Process message once all received
      header.length -= bytes;
      if(header.length == 0 && mDataCallback)
      {
        mDataCallback(this, std::move(pBuffer));
      }

      // Save leftover bytes for next iteration
      if(bytes < cacheSize)
      {
        std::move(&cache[bytes], &cache[cacheSize], &cache[0]);
        cacheSize -= bytes;
      }
      else
      {
        cacheSize = 0;
      }
    }

    // Send nullptr to signify closed
    if(!valid() && mDataCallback)
    {
      mDataCallback(this, nullptr);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // setBlocking
  /////////////////////////////////////////////////////////////////////////////
  int Socket::setBlocking(bool blocking)
  {
#ifdef _WIN32
    u_long mode = (blocking ? 0 : 1);
    return ioctlsocket(mSocketID, FIONBIO, &mode);
#else
    int mode = (blocking ? 0 : 1);
    return ioctl(mSocketID, FIONBIO, &mode);
#endif
  }

  /////////////////////////////////////////////////////////////////////////////
  // startRecvThread
  /////////////////////////////////////////////////////////////////////////////
  void Socket::startRecvThread()
  {
    mRunning = true;
    if(mServer)
    {
      mRecvThread = std::thread(&Socket::acceptThread, this);
    }
    else
    {
      mRecvThread = std::thread(&Socket::recvThread, this);
    }
  }

} // namespace Network
